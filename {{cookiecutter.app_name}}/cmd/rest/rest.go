/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package rest

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/spf13/cobra"
	"go.uber.org/zap"

	"bitbucket.com/efishery/{{cookiecutter.app_name}}/pkg/xerrors"
	"bitbucket.com/efishery/{{cookiecutter.app_name}}/pkg/xrender"
)

var (
	ENV     string
	Address string
)

// RestCmd represents the rest command.
var RestCmd = &cobra.Command{
	Use:   "rest",
	Short: "rest api for supply service",
	Run: func(cmd *cobra.Command, args []string) {
		logger, err := zap.NewProduction()
		if err != nil {
			panic(xerrors.WrapErrorf(err, xerrors.ErrorCodeUnknown, "zap.NewProduction"))
		}

		route := chi.NewRouter()

		// Add your service here
		route.Get("/", func(rw http.ResponseWriter, r *http.Request) {
			rw.Header().Add("Content-Type", "text/plain")
			xrender.Response(rw, "ok", http.StatusOK)
		})

		srv := &http.Server{
			Handler:           route,
			Addr:              fmt.Sprintf(":%s", Address),
			ReadTimeout:       1 * time.Second,
			ReadHeaderTimeout: 1 * time.Second,
			WriteTimeout:      1 * time.Second,
			IdleTimeout:       1 * time.Second,
		}

		c := make(chan os.Signal, 1)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

		go func() {
			logger.Info("Listening on port :", zap.String("address", Address))
			if err := srv.ListenAndServe(); err != nil {
				if errors.Is(err, http.ErrServerClosed) {
					log.Fatal(err)
				}
			}
		}()

		sig := <-c
		logger.Info("shutting down: %+v", zap.Any("signal", sig))

		ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
		defer cancel()

		if err := srv.Shutdown(ctx); err != nil {
			logger.Fatal("error shutingdown server", zap.Error(err))
		}
	},
}
